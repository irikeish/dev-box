###Tripock Lab quick start guide 

##### Most useful tools 
* SSH
* GIT
    * some important concept
        * git stash
        * git add -p 
        * git revert
        
* Docker
    * some important concept
        * docker image
        * docker container
        * docker mount volume
        * docker bind volume
        * docker networking
        
* Docker-compose
    
##### Frontend development
* Javascript
    * Callback
    * Closure
    * this
    * Promise
    * Async and Await
    * Event loop
    * Task queue
    
* HTML and CSS:
    * [Position](https://css-tricks.com/almanac/properties/p/position/)
    * [Display](https://css-tricks.com/almanac/properties/d/display/)
    * [Flex](https://css-tricks.com/almanac/properties/f/flex/)
    * [REM and EM](https://css-tricks.com/rem-global-em-local/)
    * [Overflow](https://css-tricks.com/almanac/properties/o/overflow/)
    * Some important concept to cover
        * [Handle network call on frontend](https://github.com/axios/axios)
        * [Debounce and Throttle](https://codeburst.io/throttling-and-debouncing-in-javascript-b01cad5c8edf)
        * [Pagination]
        * Event bubbling
        * Event propagation
    
* Framework and library
    * [React](https://reactjs.org/docs/getting-started.html)
        * Some important concept:
            * [Accessibility](https://reactjs.org/docs/accessibility.html)
            * [Code Splitting](https://reactjs.org/docs/code-splitting.html)
            * [HOC](https://reactjs.org/docs/higher-order-components.html)
            * [Refs](https://reactjs.org/docs/refs-and-the-dom.html)
    * [Redux](https://redux.js.org/tutorials/essentials/part-1-overview-concepts)
        * Some important concepts:
            * [Middleware](https://redux.js.org/advanced/middleware)
            * [Combinereducer](https://redux.js.org/recipes/structuring-reducers/using-combinereducers)
            * [Normalize state](https://redux.js.org/recipes/structuring-reducers/normalizing-state-shape)
    * [React Router for web navigation](https://reactrouter.com/web/guides/quick-start)
    * [Virtual View](https://github.com/bvaughn/react-window)
    * [Internationalization](https://formatjs.io/docs/react-intl)
    * [Antd](https://ant.design/docs/react/introduce)
    * [React Native](https://reactnative.dev/docs/getting-started)
    * [React Native navigation](https://reactnavigation.org/docs)
    

##### Backend development
* Node.js
    * [Express.js](https://expressjs.com/en/4x/api.html)
        * Some important concept
            * [middleware](https://expressjs.com/en/guide/writing-middleware.html)
            * [Helmet](https://expressjs.com/en/advanced/best-practice-security.html#use-helmet)
            * [Secure cookies](https://expressjs.com/en/advanced/best-practice-security.html#use-cookies-securely)
            * [File Upload](https://github.com/expressjs/multer)
    * [Event](https://nodejs.org/api/events.html)
    * [RBAC](https://github.com/seeden/rbac#readme)
    * [Mongoose](https://mongoosejs.com/docs)
    * [Squelize](https://sequelize.org)
    * [Joi](https://hapi.dev/tutorials/validation)
* [Laravel](https://laravel.com/docs/7.x)

* Some important concept:
    * Database Migration
    * Authentication
    * Authorization
    * CORS
    * Error handling
    * Logging
    * Schedulers and cron
    
##### Networking